import face_recognition, os, numpy as np, matplotlib.pyplot as plot

def _getEncoding(_path, _file):
	'''
	Gets the face encoding from the provided file. 
	Assumes only one face exists in the file as specified in the task.
	'''
	image = face_recognition.load_image_file(_path + "/" + _file)
	encodings = face_recognition.face_encodings(image)
	if len(encodings) < 1:
		print("Error: unable to find encoding from image:%s" % _file)
		return []
	return encodings[0]

def _compare(_target, _subject, _tolerance):
	'''
	Compares the two provided encodings using the tolerance.
	Assumes _target is only 1 encoding as it will only return the first result.
	'''
	return face_recognition.compare_faces([_target], _subject, _tolerance)[0]

def _calcScore(_target, _subject):
	'''
	Calculates the score for the comparison between the two encodings.
	'''
	return 1 - np.linalg.norm([_target] - _subject, axis=1)

def loadEncodings(_path):
	'''
	Loads all the jpg files in the provided folder into a face_recognition
	encoding array.
	'''
	encodings = []
	print("Loading files from %s..." % _path)
	# sort for easy reading of data
	for file in sorted(os.listdir(_path)):
		# make sure all files are images
		if not file.endswith(".jpg"):
			print("Skipping non image file: %s" % file)
		else:
			print("Loading encodings from file: %s" % file)
			enc = _getEncoding(_path, file)
			if len(enc) != 0:
				encodings.append( (file, enc) )
	return encodings

def compareEncodings(_encodings, _outFile = "data.csv", _tolerance = 0.6):
	'''
	Compares all of the combinations of encodings using the provided tolerance
	and outputs the results to csv in the format: probe id,subject id,score,mated.
	'''
	size = len(_encodings)
	
	# open output csv
	out = open(_outFile, 'w')
	out.write("probe id,subject id,score,mated\n")
	
	# loop through combinations
	for i in range(0, size - 1):
		pName, pEnc = _encodings[i]
		pID = pName[0:5]
		# don't need to compare against previous files
		for j in range(i + 1, size):
			sName, sEnc = _encodings[j]
			sID = sName[0:5]
			score = _calcScore(pEnc, sEnc)
			mated = _compare(pEnc, sEnc, _tolerance)
			out.write("%s,%s,%.6f,%s\n" % (pName, sName, score, mated))
	
	# flush and close the file
	out.flush()
	out.close()

def readData(_file):
	'''
	Finds and logs the number of False Non-Matches and False Matches as well as
	the average score for each. 
	'''
	file = open(_file, 'r')
	nfnm = 0 # number of false non-matches
	tfnms = 0 # total false non-match score
	nfm = 0 # number of false matches
	tfms = 0 # total false match score
	for line in file:
		if line.startswith("probe"):
			continue
		data = line.strip().split(',')
		pID = data[0][0:5]
		sID = data[1][0:5]
		score = data[2]
		mated = data[3] == 'True'
		if pID == sID: # genuine match
			if not mated:
				nfnm += 1
				tfnms += float(score)
		elif mated: # imposter!
				nfm += 1
				tfms += float(score)
	file.close()
	if nfnm > 0:
		print("False non-matches %d, avg score: %f" % (nfnm, (tfnms/nfnm)))
	if nfm > 0:
		print("False matches %d, avg score: %f" % (nfm, (tfms/nfm)))

def graphNonGenuine(_file, _tolerance):
	'''
	Graphs the False Non-Matches and False Matches in the provided .csv file
	generated from compareEncodings().
	'''
	file = open(_file, 'r')
	falseNonMatches = []
	falseMatches = []
	for line in file:
		if line.startswith("probe"):
			continue
		data = line.strip().split(',')
		pID = data[0][0:5]
		sID = data[1][0:5]
		score = float(data[2])
		mated = data[3] == 'True'
		if pID == sID: # genuine match
			if not mated:
				falseNonMatches.append(score)
		elif mated: # imposter!
				falseMatches.append(score)
	file.close()
	lfnm = "False Non-Matches %d" % len(falseNonMatches)
	lfm = "False Matches %d" % len(falseMatches)
	plot.hist([falseNonMatches, falseMatches], label=[lfnm, lfm])
	plot.xlabel("Score")
	plot.ylabel("Count")
	plot.title("Frequency of Scores using Tolerance %.2f" % _tolerance)
	plot.legend()
	plot.show()

def graphData(_file, _tolerance):
	'''
	Graphs the Genuine Matches, False Non-Matches and False Matches in the
	provided .csv file generated from compareEncodings().
	'''
	file = open(_file, 'r')
	genuineMatches = []
	falseNonMatches = []
	falseMatches = []
	for line in file:
		if line.startswith("probe"):
			continue
		data = line.strip().split(',')
		pID = data[0][0:5]
		sID = data[1][0:5]
		score = float(data[2])
		mated = data[3] == 'True'
		if pID == sID:
			if mated: # genuine match
				genuineMatches.append(score)
			else:
				falseNonMatches.append(score)
		elif mated: # imposter!
				falseMatches.append(score)
	file.close()
	lgm = "Genuine Matches %d" % len(genuineMatches)
	lfnm = "False Non-Matches %d" % len(falseNonMatches)
	lfm = "False Matches %d" % len(falseMatches)
	plot.hist([falseNonMatches, falseMatches, genuineMatches], label=[lfnm, lfm, lgm])
	plot.xlabel("Score")
	plot.ylabel("Count")
	plot.title("Frequency of Scores using Tolerance %.2f" % _tolerance)
	plot.legend()
	plot.show()
