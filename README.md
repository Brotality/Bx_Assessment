# Files:
- assessment.py: contains all the code I used to run face_recognition, generated the csv and analyse the data.

# Execution:
- Run python3 in terminal.
- import assessment.py using 'from assessment import *'.
- Load all of the encodings into an array using 'encodings = loadEncodings("./FeretMedium")'. This takes approximately 2 minutes on my VM but mary vary depending on your setup.
- Once the encodings are loaded you are able to generate csv's and analyse the data using the methods listed below.

# Methods
- loadEncodings(_path): Loads all the jpg files in the provided folder into a face_recognitionencoding array.

- compareEncodings(_encodings, _outFile = "data.csv", _tolerance = 0.6): Compares all of the combinations of encodings using the provided tolerance and outputs the results to csv in the format: probe id,subject id,score,mated.
	
- readData(_file): Finds and logs the number of False Non-Matches and False Matches as well as the average score for each.

- graphData(_file, _tolerance): Graphs the Genuine Matches, False Non-Matches and False Matches in the provided .csv file generated from compareEncodings().

- graphNonGenuine(_file, _tolerance): Graphs the False Non-Matches and False Matches in the provided .csv file generated from compareEncodings().
